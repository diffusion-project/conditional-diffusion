import torch
from torch.utils.data import Dataset
from torchvision import transforms
import os 
from PIL import Image
import pandas as pd
import numpy as np

class ConditionalDataset_AFHQ_Class(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True ):
        """
        Args:
            fpath (string):   Path to the folder where images are stored
            img_size (int):   Size of output image img_size=height=width
            ext (string):     Type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            train (Bool):     Choose dataset to be either train set or test set. frac(float) required 
        """        
        if train:
            fpath = os.path.join(fpath, 'train') 
        else:
            fpath = os.path.join(fpath, 'valid')
        
        self.class_to_idx = {'cat': 0, 'dog': 1, 'wild': 2}        

        file_list =[]
        class_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                if name.endswith(ext):
                    file_list.append(os.path.join(root, name))
                    class_list.append(self.class_to_idx[os.path.basename(root)])
        self.df = pd.DataFrame({"Filepath": file_list})
        self.class_list = class_list     
        
        if transform: 
            intermediate_size = 137
            theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*img_size))
            
            transform_rotate_flip =  transforms.Compose([transforms.ToTensor(),
                                                         transforms.Resize(intermediate_size,antialias=True),
                                                         transforms.RandomRotation((theta/np.pi*180),interpolation=transforms.InterpolationMode.BILINEAR),
                                                         transforms.CenterCrop(img_size),
                                                         transforms.RandomHorizontalFlip(p=0.5),
                                                         transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])
            
            transform_flip =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size, antialias=True),
                                                  transforms.RandomHorizontalFlip(p=0.5),
                                                  transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            self.transform =  transforms.RandomChoice([transform_rotate_flip,transform_flip])                                                   
        else :
            self.transform =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size)])

 
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        class_idx = self.class_list[idx]
        return self.transform(img), class_idx
        
    def tensor2PIL(self,img):
        back2pil = transforms.Compose([transforms.Normalize(mean=(-1,-1,-1),std=(2,2,2)),transforms.ToPILImage()])
        return back2pil(img)
    
    
class ConditionalDataset_LHQ_Paint(Dataset):
    def __init__(self,fpath,img_size,train,frac =0.8,skip_first_n = 0,ext = ".png",transform=True ):
        """
        Args:
            fpath (string):   Path to the folder where images are stored
            img_size (int):   Size of output image img_size=height=width
            ext (string):     Type of images used(eg .png)
            transform (Bool): Image augmentation for diffusion model
            train (Bool):     Choose dataset to be either train set or test set. frac(float) required 
            frac (float):     value within (0,1] (seeded)random shuffles dataset, then divides into train and test set. 
        """        
        
        self.img_size = img_size

        ### Create DataFrame
        file_list = []
        for root, dirs, files in os.walk(fpath, topdown=False):
            for name in sorted(files):
                file_list.append(os.path.join(root, name))

        df = pd.DataFrame({"Filepath":file_list},)
        self.df = df[df["Filepath"].str.endswith(ext)] 
        
        
        if train: 
            df_train = self.df.sample(frac=frac,random_state=2)
            self.df = df_train
        else:
            df_train = self.df.sample(frac=frac,random_state=2)
            df_test = df.drop(df_train.index)
            self.df = df_test

        if transform: 
            intermediate_size = 150
            theta = np.pi/4 -np.arccos(intermediate_size/(np.sqrt(2)*img_size))
            
            transform_rotate =  transforms.Compose([transforms.ToTensor(),
                                                    transforms.Resize(intermediate_size,antialias=True),
                                                    transforms.RandomRotation(theta/np.pi*180,interpolation=transforms.InterpolationMode.BILINEAR),
                                                    transforms.CenterCrop(img_size),
                                                    transforms.RandomHorizontalFlip(p=0.5),
                                                    transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])
            
            transform_randomcrop  =  transforms.Compose([transforms.ToTensor(),
                                                         transforms.Resize(intermediate_size),
                                                         transforms.RandomCrop(img_size),
                                                         transforms.RandomHorizontalFlip(p=0.5), 
                                                         transforms.Normalize(mean=(0.5,0.5,0.5),std=(0.5,0.5,0.5))])

            self.transform =  transforms.RandomChoice([transform_rotate,transform_randomcrop])                                                  
        else :
            self.transform =  transforms.Compose([transforms.ToTensor(),
                                                  transforms.Resize(img_size)])

 
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self,idx):
        # get image
        path =  self.df.iloc[idx].Filepath
        img = Image.open(path)
        # apply transformation
        img_tensor = self.transform(img)
        # draw random rectangle
        min_height = 30
        min_width = 30
        max_x = img_tensor.shape[1] - min_height
        max_y = img_tensor.shape[2] - min_width
        x = np.random.randint(0, max_x)  
        y = np.random.randint(0, max_y)
        max_height = min(self.img_size, img_tensor.shape[1] - x)
        max_width  = min(self.img_size, img_tensor.shape[2] - y) 
        rect_height = torch.randint(min_height, max_height, (1,)).item()
        rect_width  = torch.randint(min_width, max_width, (1,)).item() 
        # create copy of image and add blacked out rectangle
        masked_img = img_tensor.clone()
        masked_img[:, x:x+rect_height, y:y+rect_width] = -1 
        return img_tensor, masked_img
        
    def tensor2PIL(self,img):
        back2pil = transforms.Compose([transforms.Normalize(mean=(-1,-1,-1),std=(2,2,2)),transforms.ToPILImage()])
        return back2pil(img)
